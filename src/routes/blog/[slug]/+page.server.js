import {posts} from "$lib/data.js"
import { error } from '@sveltejs/kit';
 
/** @type {import('./$types').PageServerLoad} */
export function load({ params }) {
	const post = posts.find((post) => post.slug === params.slug);

	if (!post) throw error(404);

	return {
		post
	};
}
